import os
from forex_python.converter import CurrencyRates
from forex_python.converter import RatesNotAvailableError

def convert_currency(amount, from_currency, to_currency):
    c = CurrencyRates()
    exchange_rate = c.get_rate(from_currency, to_currency)
    converted_amount = amount * exchange_rate
    return converted_amount

def main():
    amount = float(os.environ.get("AMOUNT"))
    from_currency = os.environ.get("FROM_CURRENCY").upper()
    to_currency = os.environ.get("TO_CURRENCY").upper()

    try:
        converted_amount = convert_currency(amount, from_currency, to_currency)
        print(f"На данный момент {amount} {from_currency} равны {converted_amount} {to_currency}")
    except RatesNotAvailableError as e:
        print("Данный перевод не доступен!")


if __name__ == "__main__":
    main()
