import psycopg2

conn = psycopg2.connect(
    dbname="students",
    user="postgres",
    password="postgres",
    host="postgres",
    port="5432"
)

cur = conn.cursor()

query = """
SELECT name, exam_date FROM SubjectExam
"""
cur.execute(query)
results = cur.fetchall()
for result in results:
    print(f"Предмет: {result[0]}, Дата экзамена: {result[1]}")

cur.close()
conn.close()
